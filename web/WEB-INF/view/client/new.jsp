<jsp:include page="/WEB-INF/view/header.jsp"/>   
<div id="content">        
    <h1>Alta de cliente</h1>
        <form action="<%= request.getContextPath() %>/client/insert" method="post">
            <table>
                <tr>
                    <td>Nombre:</td>
                    <td><input type="text" name="name"></td>
                </tr>
                <tr>
                    <td>Direcci&oacute;n:</td>
                    <td><input type="text" name="address"></td>
                </tr>
                <tr>
                    <td>Tel&eacute;fono:</td>
                    <td><input type="text" name="phone"></td>
                </tr>
                <tr/>
                <tr>
                    <td>Cr&eacute;dito:</td>
                    <td><input type="text" name="credit"></td>
                </tr>
            </table>
            <input type="submit" value="Dar de alta">
        </form>
</div>
<jsp:include page="/WEB-INF/view/footer.jsp"/>
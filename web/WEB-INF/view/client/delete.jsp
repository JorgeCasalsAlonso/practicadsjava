<%@page import="model.Client"%>
<jsp:include page="/WEB-INF/view/header.jsp"/>   
<div id="content">

    <h1>Eliminaci�n de cliente</h1>

    <jsp:useBean id="client" class="model.Client" scope="request"/>
    <jsp:useBean id="count" class="Integer" scope="request"/>  

    <% if (count == 0) { %>
        <p>Cliente <%= client.getName() %> no eliminado.</p> 
    <% } else {%>
        <p>Cliente <%= client.getName() %> eliminado con �xito.</p>
    <% }%>    


</div>
<jsp:include page="/WEB-INF/view/footer.jsp"/>
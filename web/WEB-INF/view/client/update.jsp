<%@page import="model.Client"%>
<jsp:include page="/WEB-INF/view/header.jsp"/>   
<div id="content">
        <h1>Modificación de clientes</h1>
        <jsp:useBean id="client" class="model.Client" scope="request"/>  
        <jsp:useBean id="count" class="Integer" scope="request"/>  
        
        <% if (count.equals(0)) { %>
            <p>Cliente <%= client.getName() %> no actualizado</p> 
        <% } else { %>
            <p>Cliente <%= client.getName() %> actualizado.</p>
        <% } %>
</div>
<jsp:include page="/WEB-INF/view/footer.jsp"/>
<%@page import="model.Client"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<jsp:include page="/WEB-INF/view/header.jsp"/>   
<div id="content">

    <h1>Lista de clientes</h1>     
        
    <div style="float: left"><a href="<%= request.getContextPath()%>/client/new">Nuevo cliente</a></div>
    <div style="float: right"><a href="<%= request.getContextPath()%>/client/search">Buscar cliente</a></div>
    <br/>
    <jsp:useBean id="clients" class="ArrayList<model.Client>" scope="request"/>  
    <jsp:useBean id="currentPage" class="Integer" scope="request"/> 
    <jsp:useBean id="pages" class="Integer" scope="request"/>  

    <% if (pages > 0) { %>
        <% if (clients.size() > 0) { %>
            <table>
                <tr>
                    <th>Nombre</th>
                    <th>Dirección</th>
                    <th>Teléfono</th>
                    <th>Crédito</th>
                    <th>Acciones</th>
                </tr>
                <%
                    Iterator<model.Client> iterator = clients.iterator();
                    while (iterator.hasNext()) {
                        Client client= iterator.next();%>
                    <tr>
                        <td><%= client.getName() %></td>
                        <td><%= client.getAddress() %></td>
                        <td><%= client.getPhone() %></td>
                        <td><%= client.getCredit() %></td>

                        <td><a href="<%= request.getContextPath()%>/client/edit/<%= client.getId()%>">Editar</a>
                            <a href="<%= request.getContextPath()%>/client/delete/<%= client.getId()%>">Borrar</a>
                        </td>

                    </tr>
                <% } %>
            </table>
        <% } %>
        <a href="<%= request.getContextPath()%>/client/index/1">&laquo</a>&nbsp;
        <% if (currentPage>1) { %>
            <a href="<%= request.getContextPath()%>/client/index/<%= currentPage-1 %>">&lt;</a>&nbsp;
        <% } %>
        <% if (pages>currentPage) { %>
            <% for (int i = 1; i<=pages; i++) { %>
                <a href="<%= request.getContextPath()%>/client/index/<%= i %>" ><%= i %></a>&nbsp;
            <% } %>
        <% } else { %>
            <% for (int i = 1; i<= currentPage; i++) { %>
                <a href="<%= request.getContextPath()%>/client/index/<%= i %>"><%= i %></a>&nbsp;
            <% } %>
        <% } %>
        <% if (currentPage<pages) { %>
            <a href="<%= request.getContextPath()%>/client/index/<%= currentPage+1 %>" >&gt;</a>
        <% } %>
        <a href="<%= request.getContextPath()%>/client/index/<%= pages %>">&raquo</a>&nbsp;
    <% } else { %>
        <p>No hay datos que mostrar.</p>
    <% } %>
</div>
<jsp:include page="/WEB-INF/view/footer.jsp"/>
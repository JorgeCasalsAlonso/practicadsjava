<%@page import="model.Client"%>
<jsp:include page="/WEB-INF/view/header.jsp"/>   
<div id="content">

    <h1>Alta de cliente</h1>

    <jsp:useBean id="client" class="model.Client" scope="request"/>
    <jsp:useBean id="count" class="Integer" scope="request"/>  

    <% if (count == 0) { %>
        <p>Cliente <%= client.getName() %> no guardado.</p> 
    <% } else {%>
        <p>Cliente <%= client.getName() %> guardado con �xito.</p>
    <% }%>    


</div>
<jsp:include page="/WEB-INF/view/footer.jsp"/>
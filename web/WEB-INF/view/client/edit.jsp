<%@page import="model.Client"%>
<jsp:include page="/WEB-INF/view/header.jsp"/>   
<div id="content">

    <h1>Modificación de cliente</h1>

    <jsp:useBean id="client" class="model.Client" scope="request"/>  

    <form action="<%= request.getContextPath() %>/client/update" method="post">
        <input type="hidden" name="id" value="<%= client.getId() %>"/>
        <table>
            <tr>
                <td>Nombre:</td>
                <td><input type="text" name="name" value="<%= client.getName() %>"></td>
            </tr>
            <tr>
                <td>Direcci&oacute;n:</td>
                <td><input type="text" name="address" value="<%= client.getAddress() %>"></td>
            </tr>
            <tr>
                <td>Tel&eacute;fono:</td>
                <td><input type="text" name="phone" value="<%= client.getPhone() %>"></td>
            </tr>
            <tr/>
            <tr>
                <td>Cr&eacute;dito:</td>
                <td><input type="text" name="credit" value="<%= client.getCredit() %>"></td>
            </tr>
        </table>
        <input type="submit" value="Guardar cambios">
    </form>

</div>
<jsp:include page="/WEB-INF/view/footer.jsp"/>
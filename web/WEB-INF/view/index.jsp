<jsp:include page="/WEB-INF/view/header.jsp"/>   

<div id="content">
    <p>Bienvenido a la p�gina principal,
        <% 
            HttpSession sesion = request.getSession(true);
            String username = "";
            if (sesion.getAttribute("username") != null){
                username = sesion.getAttribute("username").toString();
            }
        %>
        <% if (username.isEmpty()) { %>
            invitado
        <% } else { %>
            <%= username %>
        <% } %>
    </p>
</div>

<jsp:include page="/WEB-INF/view/footer.jsp"/>
<jsp:include page="/WEB-INF/view/header.jsp"/>   
<div id="content">        
    <h1>Inicio de sesi�n</h1>
    <% 
        HttpSession sesion = request.getSession(true);
        String username = "";
        if (sesion.getAttribute("username") != null){
            username = sesion.getAttribute("username").toString();
        }
    %>
    <% if (username.isEmpty()){ %>
        <form action="<%= request.getContextPath() %>/login/in" method="post">
            <table>
                <tr>
                    <td>Nombre de usuario:</td>
                    <td><input type="text" name="username"></td>
                </tr>
            </table>
            <input type="submit" value="Iniciar sesi�n">
        </form>
    <% } else { %>
        <p>Ya has iniciado sesi�n, <%= username %>.</p>
    <% } %>
</div>
<jsp:include page="/WEB-INF/view/footer.jsp"/>
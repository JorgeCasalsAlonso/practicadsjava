<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/default.css">
    <title>Framework MVC. Curso 2016/2017</title>
</head>
<body>
<div id="header">
<div id="title">Framework MVC (Java EE). Curso 2016/2017</div>
<% 
    HttpSession sesion = request.getSession(true);
    String username = "";
    if (sesion.getAttribute("username") != null){
        username = sesion.getAttribute("username").toString();
    }
%>
<% if(username.isEmpty()){ %>
    <div id="user">Invitado</div>
<% } else { %>
    <div id="user">Usuario: <%= username %></div>
<% } %>
<br/>
<br/>
<div>
    <a href="<%= request.getContextPath() %>/index">Inicio</a>
    <a href="<%= request.getContextPath() %>/client">Clientes</a>
    <a href="<%= request.getContextPath() %>/help">Ayuda</a>
    <% if (username.isEmpty()){ %>
        <a id="session" href="<%= request.getContextPath() %>/login">Iniciar sesi&oacute;n</a>
    <% } else { %>
        <a id="session" href="<%= request.getContextPath() %>/login/out">Cerrar sesi&oacute;n</a>
    <% } %>
</div>
</div>

    

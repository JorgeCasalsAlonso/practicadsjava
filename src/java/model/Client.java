/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author Jorge
 */
public class Client implements Serializable
{
    private int id;
    private String name;
    private String address;
    private int phone;
    private double credit;

    public Client() {
        this.id = 0;
        this.name = "";
        this.address ="";
        this.phone = 0;
        this.credit = 0;
    }

    @Override
    public String toString() {
        return "Client{" + "name=" + name + ", address=" + address + ", phone=" + phone + ", credit=" + credit + "}";
    }

    
    public Client(int id, String name, String address, int phone, double credit) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.credit =  credit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }    
}

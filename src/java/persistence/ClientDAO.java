package persistence;

/*
 * Clase que se ocupa de la persistencia de CLIENT
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;
import model.Client;

/**
 *
 * @author Jorge
 */
public class ClientDAO {

    private final Object lockOfTheConexion = new Object();
    private Connection connection = null;
    private PreparedStatement stmt = null;
    private static final String CLASS_DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://192.168.81.132/practica";
    private static final Logger LOG = Logger.getLogger(ClientDAO.class.getName());
    private ResultSet rs;
    

    //metodo connect
    public void connect() {
        try {
            Class.forName(ClientDAO.CLASS_DRIVER);
            connection = DriverManager.getConnection(ClientDAO.URL, "usuario", "usuario");
//            stmt = connection.createStatement();            
            if (connection != null) {
                LOG.info("Conexión establecida!");
            } else {
                LOG.severe("Fallo de conexión!");
            }
        } catch (ClassNotFoundException ex) {
            LOG.log(Level.SEVERE,
                    "No se pudo cargar el driver de la base de datos", ex);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE,
                    "No se pudo obtener la conexión a la base de datos", ex);
        }
    }
//metodo disconnect

    public void disconnect() {
        try {
            connection.close();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }
    //un metodo por cada operación CRUD, al menos

    //insert
    /**
     *
     * @param client
     * @return int
     * @throws SQLException
     */
    public int insert(Client client) {
        try {
            stmt = connection.prepareStatement("insert into client(name, address, phone, credit) values(? ,? ,? ,?)");
            stmt.setString(1, client.getName());
            stmt.setString(2, client.getAddress());
            stmt.setInt(3, client.getPhone());
            stmt.setDouble(4, client.getCredit());
            return stmt.executeUpdate();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    public int update(Client client) {
        try {
            stmt = connection.prepareStatement("update client set name=?, address=?, phone=?, credit=? where id=?");
            stmt.setString(1, client.getName());
            stmt.setString(2, client.getAddress());
            stmt.setInt(3, client.getPhone());
            stmt.setDouble(4, client.getCredit());
            stmt.setInt(5, client.getId());
            return stmt.executeUpdate();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public ArrayList<Client> getAll() {
        ArrayList<Client> clients = null;        
        try {
            stmt = connection.prepareStatement("select * from client");
            rs = stmt.executeQuery();
            clients = new ArrayList();
            int i = 0;
            while (rs.next()) {
                i++;
                Client client = new Client();
                client.setId(rs.getInt("id"));
                client.setName(rs.getString("name"));
                client.setAddress(rs.getString("address"));
                client.setPhone(rs.getInt("phone"));
                client.setCredit(rs.getDouble("credit"));
                //other properties
                clients.add(client);
                LOG.info("Registro fila: " + i);
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return clients;
    }
    
    public ArrayList<Client> get(int page) {
        ArrayList<Client> clients = null;        
        try {
            int offset = (page-1) * 5;
            stmt = connection.prepareStatement("select * from client limit " + offset + ", " + 5);
            rs = stmt.executeQuery();
            clients = new ArrayList();
            int i = 0;
            while (rs.next()) {
                i++;
                Client client = new Client();
                client.setId(rs.getInt("id"));
                client.setName(rs.getString("name"));
                client.setAddress(rs.getString("address"));
                client.setPhone(rs.getInt("phone"));
                client.setCredit(rs.getDouble("credit"));
                //other properties
                clients.add(client);
                LOG.info("Registro fila: " + i);
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return clients;
    }
    
    public int getPageCount() {
        double count = 0;
        try{
            stmt = connection.prepareStatement("select count(id) from client");
            rs = stmt.executeQuery();
            if (rs.next()) {
                count = rs.getInt("count(id)");
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return (int) Math.ceil(count/5);
    }    
    
    public Client select(int id) {
        Client client = new Client();
        LOG.info(client.toString());
        try {
            stmt = connection.prepareStatement("select * from client where id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();            
            if (rs.next()) {
                client.setId(id);
                client.setName(rs.getString("name"));
                client.setAddress(rs.getString("address"));
                client.setPhone(rs.getInt("phone"));
                client.setCredit(rs.getDouble("credit"));
            } 
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return null;
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Falla por otra excepcion", ex);
            return null;
        }
        
        return client;
    }    
    
    public int delete(int id) {
        try {
            stmt = connection.prepareStatement("delete from client where id=?");
            stmt.setInt(1, id);
            return stmt.executeUpdate();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    public ArrayList<Client> search(String name) {
        ArrayList<Client> clients = null;        
        try {
            stmt = connection.prepareStatement("select * from client where name like ?");
            stmt.setString(1, "%" + name + "%");
            rs = stmt.executeQuery();
            clients = new ArrayList();
            int i = 0;
            while (rs.next()) {
                i++;
                Client client = new Client();
                client.setId(rs.getInt("id"));
                client.setName(rs.getString("name"));
                client.setAddress(rs.getString("address"));
                client.setPhone(rs.getInt("phone"));
                client.setCredit(rs.getDouble("credit"));
                //other properties
                clients.add(client);
                LOG.info("Registro fila: " + i);
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return clients;
    }
}
